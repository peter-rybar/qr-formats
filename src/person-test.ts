import { Person } from "./generated/person";

let peter = Person.create({
    name: "Peter",
    id: 123n, // it's a bigint
    years: 30
    // data: new Uint8Array([0xDE, 0xAD, 0xBE, 0xEF]);
});

const bytes = Person.toBinary(peter);
console.log(bytes.length, Array.prototype.toString.call(bytes));

const encoded = Buffer.from(bytes).toString('base64');
console.log(encoded.length, encoded);
const decoded = Buffer.from(encoded, 'base64');
console.log(decoded.length, Array.prototype.toString.call(decoded));

peter = Person.fromBinary(bytes);
console.log(peter);

const json = Person.toJsonString(peter);
console.log(json.length, json);
console.log(Person.toJson(peter));

peter = Person.fromJsonString('{"name":"pete", "id":"123", "baz": 31}');
console.log(peter);
