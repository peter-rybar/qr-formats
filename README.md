# QR Formats

QR formats are data structure definitions for:

- easy encode/decode - use widely adopted standard toold
- human readable - easy to implement readers and aplication integrations
- data size optimized - avoid include data attribute names

## Why new formats?

There is no official specification for QR code data format.
The QR code spec does not say anything about the contents.
Everything about the commonly-used and de facto formats and conventions
is summarized in this wiki: https://github.com/zxing/zxing/wiki/Barcode-Contents

## Format

JSON standard serialization format was used, tools are in every platform
and language.
Because of data optimization on size data are in **array notaion**,
which means semantics is done by mapping.

Format specifications are in form of:

- **Example** - example usage
- **Mapping** - data mapping and semantic description structure.
- **Code** - TypeScript was used for widly used.

## Formats

- [**pay**](formats/pay.md) - [specification](formats/pay-spec.json) - [implementation](formats/pay-impl.ts)
- [**jwt**](formats/jwt.md) - [specification](formats/jwt-spec.json)
- [**product**](formats/product.md) - [specification](formats/product-spec.json)
- [**invoice**](formats/invoice.md)

## Decode example

```typescript
const spec = ["name", "age"];
const data = ["Jim", 11];

function decodeFlat(spec: string[], data:any[]): object {
    const e = spec.map((k, i) => [k, data[i]])
    const obj = Object.fromEntries(e);
    return obj;
}

const dataObject = decodeFlat(spec, data);
console.log(dataObject);
// { name: 'Jim', age: 11 }
```

## TODO

- Create `pay` and `invoice` formats from https://bysquare.com/
- Create some documents listed in http://microformats.org/wiki/Main_Page
- https://microformats.org/wiki/Category:Draft_Specifications
