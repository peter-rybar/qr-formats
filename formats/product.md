# QR Product

## Examples

```json
["qr:product","Product name","Product description","tag1, tag 2",123.45,"EUR","CW21001"]

[
    "qr:product",
    "Product name",
    "Product description",
    "tag1, tag 2",
    123.45,
    "EUR",
    "CW21001"
    "http://products.com/p1",
    "http://products.com/p1.png"
]
```

## Mapping

```javascript
[
    "qr:product",
    "name:string",
    "description:string",
    "tags:string:comma_separated_list",
    "price:number:min=0",
    // https://en.wikipedia.org/wiki/ISO_4217
    "currency:string:regex=[A-Za-z]{3}",
    "sku:string",
    "url:string?:url",
    "img:string?:url"
]
```

## Code

```typescript
interface QrProduct {
    name: string;
    description: string;
    tags: string;
    price: number;
    currency: string;
    sku: string;
    url: string;
    img: string;
}
```

[Specification](product-spec.json)

[QR Code example](https://chart.googleapis.com/chart?cht=qr&chs=500&chld=L%7C1&chl=[%22qr:product%22,%22Product%20name%22,%22Product%20description%22,%22tag1,%20tag%202%22,123.45,%22EUR%22,%22CW21001%22])
