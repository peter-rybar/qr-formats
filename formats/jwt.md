# QR JWT

## Examples

```json
[
    "qr:jwt",
    "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c",
    {"sub":"1234567890","name":"John Doe","iat":1516239022}
]

[
    "qr:jwt",
    "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c"
]
```

## Mapping

```javascript
[
    "qr:jwt",
    "jwt:string", // mandatory
    "payload:object?"
]
```

## Code

```typescript
interface QrJwt {
    jwt: string;
    payload: string;
}
```

[Specification](jwt-spec.json)

[QR Code example](https://chart.googleapis.com/chart?cht=qr&chs=500&chld=L%7C1&chl=[%22qr:jwt%22,%22eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c%22,{%22sub%22:%221234567890%22,%22name%22:%22John%20Doe%22,%22iat%22:1516239022}])

[QR Code example](https://chart.googleapis.com/chart?cht=qr&chs=500&chld=L%7C1&chl=[%22qr:jwt%22,%22eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c%22])
