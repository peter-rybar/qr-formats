# QR Pay

## Examples

```json
["qr:pay","SK0809000000000123123123",123.45,"EUR"]

[
    "qr:pay",
    "SK0809000000000123123123",
    123.45,
    "EUR",
    "FIOZSKBAXXX",
    "1234567890",
    "1234",
    "1234567890",
    "2022-01-23",
    "Street",
    "City",
    "SEPA ID",
    "My payment",
    "Payee"
]
```

## Mapping

```javascript
[
    "qr:pay",
    // https://en.wikipedia.org/wiki/International_Bank_Account_Number
    // mandatory
    "iban:string:regex=^[a-zA-Z]{2}.*$",
    // mandatory
    "amount:number:min=0",
    // https://en.wikipedia.org/wiki/ISO_4217
    // mandatory
    "currency:string:regex=[A-Za-z]{3}",
    // https://en.wikipedia.org/wiki/ISO_9362
    "bic:string?:regex=^[A-Z]{4}(AC|AD|AE|AF|AG|AI|AL|AM|AN|AO|AQ|AR|AS|AT|AU|AW|AX|AZ|BA|BB|BD|BE|BF|BG|BH|BI|BJ|BL|BM|BN|BO|BQ|BR|BS|BT|BV|BW|BY|BZ|CA|CC|CD|CE|CF|CG|CH|CI|CK|CL|CM|CN|CO|CP|CR|CS|CU|CV|CW|CX|CY|CZ|DD|DE|DG|DJ|DK|DM|DO|DZ|EA|EC|EE|EG|EH|ER|ES|ET|EU|FI|FJ|FK|FM|FO|FR|FX|GA|GB|GD|GE|GF|GG|GH|GI|GL|GM|GN|GP|GQ|GR|GS|GT|GU|GW|GY|HK|HM|HN|HR|HT|HU|IC|ID|IE|IL|IM|IN|IO|IQ|IR|IS|IT|JE|JM|JO|JP|KE|KG|KH|KI|KM|KN|KP|KR|KW|KY|KZ|LA|LB|LC|LI|LK|LR|LS|LT|LU|LV|LY|MA|MC|MD|ME|MF|MG|MH|MK|ML|MM|MN|MO|MP|MQ|MR|MS|MT|MU|MV|MW|MX|MY|MZ|NA|NC|NE|NF|NG|NI|NL|NO|NP|NR|NT|NU|NZ|OM|PA|PE|PF|PG|PH|PK|PL|PM|PN|PR|PS|PT|PW|PY|QA|RE|RO|RS|RU|RW|SA|SB|SC|SD|SE|SF|SG|SH|SI|SJ|SK|SL|SM|SN|SO|SR|SS|ST|SU|SV|SX|SY|SZ|TA|TC|TD|TF|TG|TH|TJ|TK|TL |TM|TN|TO|TP|TR|TT|TV|TW|TZ|UA|UG|UK|UM|US|UY|UZ|VA|VC|VE|VG|VI|VN|VU|WF|WS|XK|YE|YT|ZA|ZM|ZR|ZW)[2-9A-Z][0-9A-NP-Z](XXX|[0-9A-WYZ][0-9A-Z]{2})?$",
    // up to 10 digits
    "variable_symbol:string?:regex=^[0-9]{0,10}$",
    // 4 digits
    "constant_symbol:string?:regex=^[0-9]{4}$",
    // up to 10 digits
    "specific_symbol:string?:regex=^[0-9]{0,10}$",
    // https://en.wikipedia.org/wiki/ISO_8601
    "due_date:string?:regex=^([0-9]{4})-([0-9]{2})-([0-9]{2})$",
    "beneficiary_address_line_1:string?",
    "beneficiary_address_line_2:string?",
    //  info for beneficiaries by SEPA
    "payers_reference:string?",
    // note for recipient
    "payment_note:string?:max=140",
    "beneficiary_name:string?"
]
```

## Code

```typescript
interface QrPay {
    iban: string;
    amount: number;
    currency: string;
    bic: string;
    variable_symbol: string;
    constant_symbol: string;
    specific_symbol: string;
    due_date: string;
    beneficiary_address_line_1: string;
    beneficiary_address_line_2: string;
    payers_reference: string;
    payment_note: string;
    beneficiary_name: string;
}
```

[Specification](pay-spec.json)

[Implementation](pay-impl.ts)

[QR Code example](https://chart.googleapis.com/chart?cht=qr&chs=500&chld=L|1&chl=[%22qr:pay%22,%22SK0809000000000123123123%22,123.45,%22EUR%22])
