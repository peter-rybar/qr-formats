
function decodeFlat(spec: string[], data:any[]): object {
    const entries = spec.map((k, i) => [k, data[i]])
    const obj = Object.fromEntries(entries);
    return obj;
}

class Pay {
    private static spec = [
        "qr:pay",
        // https://en.wikipedia.org/wiki/International_Bank_Account_Number
        // mandatory
        "iban:string:regex=^[a-zA-Z]{2}.*$",
        // mandatory
        "amount:number:min=0",
        // https://en.wikipedia.org/wiki/ISO_4217
        // mandatory
        "currency:string:regex=[A-Za-z]{3}",
        // https://en.wikipedia.org/wiki/ISO_9362
        "bic:string?:regex=^[A-Z]{4}(AC|AD|AE|AF|AG|AI|AL|AM|AN|AO|AQ|AR|AS|AT|AU|AW|AX|AZ|BA|BB|BD|BE|BF|BG|BH|BI|BJ|BL|BM|BN|BO|BQ|BR|BS|BT|BV|BW|BY|BZ|CA|CC|CD|CE|CF|CG|CH|CI|CK|CL|CM|CN|CO|CP|CR|CS|CU|CV|CW|CX|CY|CZ|DD|DE|DG|DJ|DK|DM|DO|DZ|EA|EC|EE|EG|EH|ER|ES|ET|EU|FI|FJ|FK|FM|FO|FR|FX|GA|GB|GD|GE|GF|GG|GH|GI|GL|GM|GN|GP|GQ|GR|GS|GT|GU|GW|GY|HK|HM|HN|HR|HT|HU|IC|ID|IE|IL|IM|IN|IO|IQ|IR|IS|IT|JE|JM|JO|JP|KE|KG|KH|KI|KM|KN|KP|KR|KW|KY|KZ|LA|LB|LC|LI|LK|LR|LS|LT|LU|LV|LY|MA|MC|MD|ME|MF|MG|MH|MK|ML|MM|MN|MO|MP|MQ|MR|MS|MT|MU|MV|MW|MX|MY|MZ|NA|NC|NE|NF|NG|NI|NL|NO|NP|NR|NT|NU|NZ|OM|PA|PE|PF|PG|PH|PK|PL|PM|PN|PR|PS|PT|PW|PY|QA|RE|RO|RS|RU|RW|SA|SB|SC|SD|SE|SF|SG|SH|SI|SJ|SK|SL|SM|SN|SO|SR|SS|ST|SU|SV|SX|SY|SZ|TA|TC|TD|TF|TG|TH|TJ|TK|TL |TM|TN|TO|TP|TR|TT|TV|TW|TZ|UA|UG|UK|UM|US|UY|UZ|VA|VC|VE|VG|VI|VN|VU|WF|WS|XK|YE|YT|ZA|ZM|ZR|ZW)[2-9A-Z][0-9A-NP-Z](XXX|[0-9A-WYZ][0-9A-Z]{2})?$",
        // up to 10 digits
        "variable_symbol:string?:regex=^[0-9]{0,10}$",
        // 4 digits
        "constant_symbol:string?:regex=^[0-9]{4}$",
        // up to 10 digits
        "specific_symbol:string?:regex=^[0-9]{0,10}$",
        // https://en.wikipedia.org/wiki/ISO_8601
        "due_date:string?:regex=^([0-9]{4})-([0-9]{2})-([0-9]{2})$",
        "beneficiary_address_line_1:string?",
        "beneficiary_address_line_2:string?",
        //  info for beneficiaries by SEPA
        "payers_reference:string?",
        // note for recipient
        "payment_note:string?:max=140",
        "beneficiary_name:string?"
    ];

    static qrDecode(qrPayData: string) {
        if (!qrPayData.startsWith(`["qr:`)) {
            throw new Error("Not a QR Format");
        }
        const qrPay = JSON.parse(qrPayData);
        if (qrPay[0] !== "qr:pay") {
            throw new Error("Not a QR Format type: pay");
        }
        const keys = Pay.spec.map(k => k.split(":")[0]);
        return decodeFlat(keys, qrPay) as Pay;
    }

    iban?: string;
    amount?: number;
    currency?: string;
    bic?: string;
    variable_symbol?: string;
    constant_symbol?: string;
    specific_symbol?: string;
    due_date?: string;
    beneficiary_address_line_1?: string;
    beneficiary_address_line_2?: string;
    payers_reference?: string;
    payment_note?: string;
    beneficiary_name?: string;
}

// const qrData = `["qr:pay","SK0809000000000123123123",123.45,"EUR"]`;
const qrData = `["qr:pay","SK0809000000000123123123",123.45,"EUR","FIOZSKBAXXX","1234567890","1234","1234567890","2022-01-23","Street","City","SEPA ID","My payment","Payee"]`;

const pay = Pay.qrDecode(qrData);
console.dir(pay);
// {
//     iban: 'SK0809000000000123123123',
//     amount: 123.45,
//     currency: 'EUR',
//     bic: 'FIOZSKBAXXX',
//     variable_symbol: '1234567890',
//     constant_symbol: '1234',
//     specific_symbol: '1234567890',
//     due_date: '2022-01-23',
//     beneficiary_address_line_1: 'Street',
//     beneficiary_address_line_2: 'City',
//     payers_reference: 'SEPA ID',
//     payment_note: 'My payment',
//     beneficiary_name: 'Payee'
// }
