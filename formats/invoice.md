# QR Invoice - TODO

## Example

```json
[
    "qr:invoice",
    ["name 1", "address 1"],
    ["name 2", "address 2"],
    [
        ["invoice item 1", 2, 13],
        ["invoice item 2", 11, 1]
    ],
    1000.50,
    200.45
]
```

## Mapping

```javascript
[
    "qr:invoice",
    // supplier
    [
        "name:string",
        "address: string"
    ],
    // customer
    [
        "name:string",
        "address: string"
    ],
    // items
    [
        [
            "name:string",
            "price:number",
            "count:number"
        ]
    ],
    "price:number",
    "wat:number"
]
```

## Code

```typescript
interface Invoice {
    supplier: {
        name: string;
        address: string;
    };
    customer: {
        name: string;
        address: string;
    };
    items: Array<{
        name: string;
        price: number;
        count: number;
    }>;
    price: numbe;
    wat: number;
}
```

[QR Code example](https://chart.googleapis.com/chart?cht=qr&chs=500&chld=L%7C1&chl=[%22qr:invoice%22,[%22from%22,%22address%22],[%22to%22,%22address%22],[[%22invoice%20item%201%22,2,13],[%22invoice%20item%202%22,11,1]],1.000,200])
